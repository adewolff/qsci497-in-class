import numpy as np


class Agent:
    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__()

    def __init__(self, position=None, starting_energy=1, world=None, reproduction_threshold=100, max_age=100):
        self.position = position
        self.energy = starting_energy
        self.world = world
        self.reproduction_threshold = reproduction_threshold
        self.age = 0
        self.max_age = max_age + np.random.randint(1, 50)

    def move(self, new_position: list):
        """
        Moves the agent to specified position
        :param new_position: grid coordinates the agent should move to
        """
        if self.world:
            new_position = self.world.world_wrap(new_position)

        self.position = new_position
        self.energy -= 1

    def random_move(self, kill_on_zero=False):
        """
        Moves the agent to a random surrounding position.
        :param kill_on_zero: Whether or not the function should automatically kill agents with 0 energy
        """
        if kill_on_zero:
            self._no_energy_kill_()

        y_move = np.random.randint(-1, 2)
        x_move = np.random.randint(-1, 2)

        self.move([self.position[0] + y_move, self.position[1] + x_move])

    def _no_energy_kill_(self):
        """
        Kills agent when it runs out of energy
        """
        if self.energy <= 0:
            self._die_()

    def _die_(self):
        """
        Marks the agent for death, to be removed by the remove_carcass function
        """
        self.world.kill_list.add(self)

    def _age_(self):
        """
        ages the agent by a year
        """
        self.age += 1

    def _old_age_(self):
        """
        Kills the agent if it reaches its maximum age
        """
        if self.age > self.max_age:
            self._die_()

    def _get_surroundings_(self):
        """
        Finds the grids surrounding the agent
        :return: List of coordinates
        """
        surroundings = list()

        for i in range(-1, 2):
            for j in range(-1, 2):
                if not i and not j:  # Don't add the original position to the list
                    pass
                else:
                    surroundings.append([self.position[0] + i, self.position[1] + j])

        return surroundings


class Sheep(Agent):
    def __init__(self, position=None, starting_energy=1, world=None, scent_strength=1, grass_gain=1,
                 reproduction_threshold=100):
        super().__init__(position, starting_energy, world, reproduction_threshold)
        self.scent_strength = scent_strength
        self.grass_gain = grass_gain

    def get_eaten(self):
        """
        Kills the Sheep and returns its energy
        :return: Sheep's energy level
        """
        self._die_()
        return self.energy

    def grass_move(self, kill_on_zero=False, leave_scent=True, age=False):
        """
        Moves the Sheep to the surrounding plot with the most grass.
        :param kill_on_zero: Whether or not the function should automatically kill agents with 0 energy
        :param leave_scent: Whether or not the agent should leave a scent trail
        :param age: Whether or not the agent should age and die of old age
        """
        if kill_on_zero:
            self._no_energy_kill_()

        if age:
            self._age_()
            self._old_age_()

        new_pos = self._highest_grass_()
        self.move(new_pos)
        self._eat_grass_()

        if leave_scent:
            self._leave_scent_()

    def _highest_grass_(self):
        """
        Finds the highest amount of grass surrounding the sheep
        :return: list of grid coordinates
        """
        surroundings = self._get_surroundings_()
        nearby_grass = dict()

        for i in surroundings:
            i = self.world.world_wrap(i)

            grass_amt = self.world.grass_field[i[1], i[0]]
            nearby_grass[grass_amt] = i

        return nearby_grass[max(nearby_grass)]

    def _eat_grass_(self):
        """
        Remove grass from world at current position and gain energy depending on grass eaten
        """
        grass_amt = self.world.grass_field[self.position[1], self.position[0]]

        self.world.grass_field[self.position[1], self.position[0]] = 0

        self.energy += grass_amt * self.grass_gain

    def reproduce_asexual(self):
        """
        Sheep reproduces asexually.
        """
        if self.energy > self.reproduction_threshold:
            baby_birth_spot = [self.position[0] + np.random.randint(-1, 2), self.position[1] + np.random.randint(-1, 2)]
            baby_scent_strength = self.scent_strength + np.random.randint(-2, 3) / 10
            if baby_scent_strength < 0:
                baby_scent_strength = 0

            self.energy -= self.reproduction_threshold

            return Sheep(baby_birth_spot, starting_energy=10, scent_strength=baby_scent_strength, world=self.world,
                         reproduction_threshold=self.reproduction_threshold)

    def _leave_scent_(self):
        """
        Sheep leaves a scent at its current position
        """
        x = self.position[1]
        y = self.position[0]

        self.world.scent_field[x][y] += self.scent_strength


class Wolf(Agent):
    def __init__(self, position=None, starting_energy=10, world=None, move_speed=1):
        super().__init__(position, starting_energy, world)
        self.move_speed = move_speed

    def hunt(self):
        """
        tracks sheep by scent trail, and eats any adjacent sheep.
        """
        for i in range(self.move_speed):
            adjacent_sheep = self.find_sheep()
            if len(adjacent_sheep) > 0:
                target = adjacent_sheep[0]
                target_id = self.world.on_grid(target).pop()

                self.move(target)
                self.eat(target_id)
            else:
                trail = self.find_scent()
                if len(trail) > 0:
                    self.move(trail)
                else:
                    self.random_move()

    def eat(self, prey):
        """
        Kills a sheep and gains the sheep's energy level
        :param prey: Sheep to eat
        """
        # Eating a malnourished sheep will result in an energy gain of 4
        self.energy += abs(prey.get_eaten() - 5)

    def find_sheep(self):
        """
        Finds nearby grids that contain sheep
        :return: list of grid coordinates
        """
        surroundings = self._get_surroundings_()
        nearby_agents = list(map(self.world.on_grid, surroundings))

        nearby_sheep = list()
        for i in nearby_agents:
            for j in i:
                if type(j).__name__ == 'Sheep':
                    nearby_sheep.append(j.position)

        return nearby_sheep

    def find_scent(self):
        """
        Finds the strongest scent in the surrounding area
        :return: grid coordinate
        """
        surroundings = self._get_surroundings_()

        nearby_scent = list()
        strongest_scent = 0
        for i in surroundings:
            i = self.world.world_wrap(i)
            if self.world.scent_field[i[1], i[0]] > strongest_scent:
                strongest_scent = self.world.scent_field[i[1], i[0]]
                nearby_scent = i

        return nearby_scent
