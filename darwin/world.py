import numpy as np


class World:
    def __init__(self, length, width, growth_speed=0.1, max_grass=20, scent_decay_rate=1):
        self.length = length
        self.width = width
        self.dimension = self.length * self.width
        self.size = [self.length, self.width]
        self.scent_field = np.zeros((self.width + 1, self.length + 1))
        self.grass_field = np.random.rand(self.width + 1, self.length + 1) * 10
        self.agents = dict()
        self.growth_speed = growth_speed
        self.max_grass = max_grass
        self.scent_decay_rate = scent_decay_rate
        self.kill_list = set()

    def add_agents(self, agent_name, new_agents):
        self.agents[agent_name].add(new_agents)

    def on_grid(self, search_location: list):
        """
        Returns any agents that are on the specified world grid
        :param agent_type: type of agent to search for (e.g., Sheep or Wolves)
        :param search_location: grid location to be searched
        :return: set of agents present on grid
        """
        assert self.agents, "World does not contain any agents"

        presence = set()

        for agent in self.agents["sheep"]:
            if agent.position == search_location:
                presence.add(agent)

        return presence

    def remove_carcass(self):
        """
        Removes marked for death sheep from the world
        """
        for i in self.kill_list:
            self.agents["sheep"].remove(i)
            i.position = None
            if i.world:
                i.world = None
        self.kill_list = set()

    def average_scent_strength(self):
        scent_sum = 0
        for i in self.agents["sheep"]:
            scent_sum += i.scent_strength

        return scent_sum / len(self.agents["sheep"])

    def grow_grass(self):
        """
        Grows grass according to growth speed with an added randomization
        """
        self.grass_field[self.grass_field < self.max_grass] += abs(self.growth_speed)

    def scent_decay(self):
        """
        decays scents in the scent field
        """
        self.scent_field[self.scent_field > 0] -= self.scent_decay_rate

    def world_wrap(self, coord: list):
        """
        Wraps coordinates which are out of bounds around the world
        :param coord: coordinate to check for out of bound and potentially wrap
        :return:
        """
        for i in range(len(coord)):
            if coord[i] > self.size[i]:
                coord[i] = 0 + coord[i] - 1 - self.size[i]
            elif coord[i] < 0:
                coord[i] = self.size[i] - abs(coord[i]) + 1

        return coord
