import sqlite3


def create_database(database_name):
    """Creates SQLite database"""

    print("Creating new database...")

    with sqlite3.connect('{}.sqlite3'.format(database_name)):
        pass

    print("SQLite database {} created.".format(database_name))


def add_data(database_name, experiment_name, replicate, iteration, pop_avg_scent):
    """Writes fetched email to SQLite database"""

    assert len(iteration) == len(pop_avg_scent), "Data are not equal in length"

    with sqlite3.connect('{}.sqlite3'.format(database_name)) as conn:
        cur = conn.cursor()
        for i in range(len(iteration)):
            id = str(replicate) + '_' + str(iteration[i])

            sql_insertion = 'INSERT INTO {}(id, replicate, iteration, pop_avg_scent) VALUES (?, ?, ?, ?)'.format(
                experiment_name)
            cur.execute(sql_insertion, (id, replicate, iteration[i], pop_avg_scent[i]))
        conn.commit()


def experiment_exists(database_name, experiment_name):
    """Check if an experiment table already exists"""
    with sqlite3.connect('{}.sqlite3'.format(database_name)) as conn:

        sql_query = "SELECT name FROM sqlite_master WHERE type='table' AND name='{}'".format(experiment_name)

        cur = conn.cursor()
        cur.execute(sql_query)

        res = cur.fetchall()

        if len(res):
            return True
        else:
            return False


def add_experiment(database_name, experiment_name):
    """Adds a new table to store a new experiment"""

    table_sql = """
    CREATE TABLE {}(
    id TEXT PRIMARY KEY NOT NULL,
    iteration INT NOT NULL,
    replicate INT NOT NULL,
    pop_avg_scent INT NOT NULL
    );
    """.format(experiment_name)

    with sqlite3.connect('{}.sqlite3'.format(database_name)) as conn:
        cur = conn.cursor()
        cur.execute(table_sql)

    print("Table {} added to SQLite database {}.".format(experiment_name, database_name))


def num_replicates(database_name, experiment_name):
    """Retrieves the number of existing replicates for a certain experiment"""

    with sqlite3.connect('{}.sqlite3'.format(database_name)) as conn:
        cur = conn.cursor()

        sql_quer = "SELECT MAX(replicate) FROM {}".format(experiment_name)
        cur.execute(sql_quer)
        max_repl = cur.fetchall()

        if max_repl == [(None,)]:
            max_repl = 0
            return max_repl
        return max_repl[0][0]
