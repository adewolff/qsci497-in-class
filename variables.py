num_sheep = 30
num_wolves = 15

exp_iterations = 30000
exp_replicates = 26

repro_threshold = 900
wolf_speed = 3
scent_lower = 2
scent_upper = 5

database_name = "darwin_results"

exp_identifier = "i{}s{}w{}re{}sl{}su{}".format(exp_iterations, num_sheep, num_wolves, repro_threshold, scent_lower,
                                                scent_upper)
