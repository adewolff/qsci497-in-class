import numpy as np
import matplotlib.pyplot as plt
import darwin as dw
from variables import *
import os

make_plots = False
make_plots_iteration = True
plot_iterations = 100

total_scent_averages = list()

dw.storage.create_database(database_name)
if not dw.storage.experiment_exists(database_name, exp_identifier):
    dw.storage.add_experiment(database_name, exp_identifier)
    existing_replicates = -1
else:
    existing_replicates = dw.storage.num_replicates(database_name, exp_identifier)

for replicates in range(exp_replicates):

    Narnia = dw.World(100, 100)

    Narnia.agents["sheep"] = set(dw.agents.Sheep([np.random.randint(0, Narnia.length - 1),
                                                  np.random.randint(0, Narnia.width - 1)],
                                                 world=Narnia, scent_strength=np.random.randint(scent_lower, scent_upper),
                                                 reproduction_threshold=repro_threshold)
                                 for i in range(num_sheep))

    Narnia.agents["wolves"] = set(dw.agents.Wolf([np.random.randint(0, Narnia.length - 1),
                                                  np.random.randint(0, Narnia.width - 1)],
                                                 world=Narnia, move_speed=wolf_speed)
                                  for i in range(num_wolves))

    iteration = 0
    scent_avgs = list()
    for i in range(exp_iterations):
        Narnia.grow_grass()
        Narnia.scent_decay()
        Narnia.remove_carcass()

        # Sheep:
        baby_sheep = set()
        for sheep in Narnia.agents["sheep"]:
            sheep.grass_move(age=False)
            baby_sheep.add(sheep.reproduce_asexual())

        for sheep in baby_sheep:
            if sheep:
                Narnia.add_agents("sheep", sheep)

        # Wolves:
        for wolf in Narnia.agents["wolves"]:
            wolf.hunt()

        # Visualization
        if make_plots:
            if make_plots_iteration and iteration % plot_iterations != 0:
                pass
            else:
                plt.figure(1)
                grass = plt.imshow(Narnia.grass_field, cmap='YlGn', interpolation='nearest', vmin=0.0, vmax=20.0)
                SHP = plt.scatter([x.position[0] for x in Narnia.agents["sheep"]],
                                  [y.position[1] for y in Narnia.agents["sheep"]], color='b')
                WLF = plt.scatter([x.position[0] for x in Narnia.agents["wolves"]],
                                  [y.position[1] for y in Narnia.agents["wolves"]], color='r')
                plt.show()
                if not make_plots_iteration:
                    plt.pause(0.1)
                SHP.remove()
                WLF.remove()
                grass.remove()

        # scent strength monitoring
        try:
            scent_avgs.append(Narnia.average_scent_strength())
        except ZeroDivisionError:
            break

        print(iteration)
        iteration += 1

    total_scent_averages.append(scent_avgs)

    # Adaptation plotting
    # plt.scatter([i for i in range(1, iteration + 1)], scent_avgs)
    # plt.xlabel("iterations")
    # plt.ylabel("sheep population average scent strength")
    # plt.title("population scent strength over time. Experiment: {}, replicate: {}".format(exp_identifier, replicates))
    # if not os.path.exists("plots/{}".format(exp_identifier)):
    #     os.mkdir("plots/{}".format(exp_identifier))
    # plt.savefig("plots/{}/replicate_{}.png".format(exp_identifier, replicates))
    # plt.close()

    # store in sql:
    total_replicate = existing_replicates + replicates + 1
    dw.storage.add_data(database_name, exp_identifier, total_replicate, [i for i in range(1, iteration + 1)],
                        scent_avgs)

# Final cumulative plot
# for i in range(len(total_scent_averages)):
#     plt.scatter([i for i in range(1, len(total_scent_averages[i]) + 1)], total_scent_averages[i],
#                 label="replicate {}".format(i))
#     plt.xlabel("iterations")
#     plt.ylabel("sheep population average scent strength")
#     plt.title("population scent strength over time. Experiment: {}".format(exp_identifier))
#     plt.legend()
# plt.savefig("plots/{}/all_replicates.png".format(exp_identifier))
# plt.close()
